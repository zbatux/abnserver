import express from 'express';
import http from 'http';
import socketio from 'socket.io';
import uuidv4 from 'uuid/v4';
import {removeElement, datedLog} from './misc/utils';
import UniqueTuple from './misc/uniqueTuple';
import Game from './game'

export default class Server {
    // constructor
    constructor() {
        this.server = http.Server(express());
        this.websocket = socketio(this.server,{'pingInterval': 3000, 'pingTimeout': 5000});
        this.usersToSockets = new UniqueTuple('userId', 'socket');
        this.queuedUsers = [];
        this.playingUsers = [];
        this.playingPairs = new UniqueTuple('player1Id', 'player2Id');
        this.gameRooms = new UniqueTuple('roomId', 'game');

        this.websocket.on('connection', socket => {
            datedLog('A client just joined on ' + socket.id);

            socket.emit('welcome', this.onNewConnection(socket));
            socket.on('join room', roomId => socket.join(roomId));
            socket.on('turn played', (figure, roomId, playerId) => this.checkTurnValidity(socket, figure, roomId, playerId));
            socket.on('wanaplay', userId => this.newChallenger(userId));
            socket.on('ask playerCount', () => this.sendPlayerCount(socket));
            socket.on('disconnect', () => this.removeDisconnected(socket));
            socket.on('delete game', (roomId) => this.removeGame(roomId));
        });
        const port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
        const ip_address = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

        this.server.listen(port, ip_address, () => datedLog('Server started on ' + ip_address + ':' + port));
    }

    close() {
        this.server.close();
        this.usersToSockets = new UniqueTuple();
        this.queuedUsers = [];
        this.playingUsers = [];
        this.playingPairs = new UniqueTuple();
    }

    onNewConnection(socket) {
        const userId = uuidv4();
        this.usersToSockets.set(userId, socket, 'userId');
        return userId;
        //console.log('[Serveur] Users connected: ', this.usersToSockets.size, ' Queued users: ', this.queuedUsers.length, ' Users playing: ', this.playingUsers.length, ' Running Games: ', this.playingPairs.size);

    }

    newChallenger(userId) {
        datedLog(userId + ' wants to play');
        if(userId === null) return;
        if (this.queuedUsers.length < 1) {
            this.queuedUsers.push(userId);
            this.usersToSockets.get(userId).emit('waipting For Opponent');
        }
        else {
            let player1 = this.queuedUsers.pop();
            this.launchgame(player1, userId);
        }
        //console.log('[Serveur] Users connected: ', this.usersToSockets.size, ' Queued users: ', this.queuedUsers.length, ' Users playing: ', this.playingUsers.length, ' Running Games: ', this.playingPairs.size);

    }

    checkTurnValidity(socket,figure, roomId, playerId){
        if(!this.gameRooms.has(roomId)){
            socket.emit('reconnect');
            return
        }
        this.computeTurn(figure, roomId, playerId);
    }

    computeTurn(figure, roomId, playerId) {

        let game = this.gameRooms.get(roomId);
        game.registerPlayerTurn(figure, playerId);
        datedLog(playerId + ' Turn registered');
        if (game.winner == null) {
            if (game.turnScore[0] != null && game.turnScore[1] != null) {
                if (game.turnScore[0] > game.turnScore[1]) {
                    //console.log('p1 win');
                    this.tellTurnWon(game.player1, game);
                    this.tellTurnLost(game.player2, game);
                }
                else if (game.turnScore[0] < game.turnScore[1]) {
                    //console.log('p2 win');
                    this.tellTurnWon(game.player2, game);
                    this.tellTurnLost(game.player1, game);
                }
                else {
                    this.tellTurnDraw(game.player1, game);
                    this.tellTurnDraw(game.player2, game);
                }
                game.turnScore = [null, null];
            }
        }
        else this.endGame(roomId);
    }

    endGame(roomId) {
        let game = this.gameRooms.get(roomId);
        datedLog('The winner of game ' + roomId + ' is ' + game.winner);
        this.usersToSockets.get(game.winner).emit('Game won', game.getGameStateForPlayer(game.winner));
        this.usersToSockets.get(this.playingPairs.get(game.winner)).emit('Game lost', game.getGameStateForPlayer(this.playingPairs.get(game.winner)));
        this.playingPairs.delete(game.player1);
        removeElement(this.playingUsers, game.player1);
        removeElement(this.playingUsers, game.player2);
        this.gameRooms.delete(roomId);
    }

    tellTurnWon(playerId, game) {
        datedLog(playerId + ' won the turn');
        this.usersToSockets.get(playerId).emit('Turn won', game.getGameStateForPlayer(playerId));
    }

    tellTurnLost(playerId, game) {
        datedLog(playerId + ' lost the turn');
        this.usersToSockets.get(playerId).emit('Turn lost', game.getGameStateForPlayer(playerId));
    }

    tellTurnDraw(playerId, game) {
        datedLog('Turn draw');
        this.usersToSockets.get(playerId).emit('Turn draw', game.getGameStateForPlayer(playerId));
    }

    launchgame(player1, player2) {
        this.playingPairs.set(player1, player2, 'player1Id');
        this.playingUsers.push(player1, player2);
        let player1Socket = this.usersToSockets.get(player1);
        let player2Socket = this.usersToSockets.get(player2);
        const roomId = uuidv4();
        let newGame = new Game(player1, player2, roomId);
        this.gameRooms.set(roomId, newGame, 'roomId');
        player1Socket.emit('Opponent found', {
            opponentId: player2,
            roomId: roomId
        });
        player2Socket.emit('Opponent found', {
            opponentId: player1,
            roomId: roomId
        });
        datedLog('A new game started ', player1, 'vs ', player2, "GameRoom: ", roomId);
        console.log('[Serveur] Users connected: ', this.usersToSockets.size, ' Queued users: ', this.queuedUsers.length, ' Users playing: ', this.playingUsers.length, ' Running Games: ', this.playingPairs.size);

    }

    removeDisconnected(socket) {
        const userId = this.usersToSockets.get(socket);
        if (this.playingUsers.indexOf(userId) !== -1) {
            const opponentId = this.playingPairs.get(userId);
            this.usersToSockets.get(opponentId).emit('Opponent disconnected');
            removeElement(this.playingUsers, opponentId);
            this.playingPairs.delete(userId);
        }
        this.usersToSockets.delete(userId);
        removeElement(this.queuedUsers, userId);
        removeElement(this.playingUsers, userId);
        datedLog(userId + ' left the App');
    };

    removeGame(roomId) {
        if(this.gameRooms.has(roomId))
            this.gameRooms.delete(roomId);
    }

    getServerStatus(){
        datedLog('Users connected: ' + this.usersToSockets.size + ' Queued users: ' + this.queuedUsers.length + ' Users playing: ' + this.playingUsers.length + ' Running Games: ' + this.playingPairs.size);
    }

    sendPlayerCount(socket) {
        socket.emit('playerCount', this.usersToSockets.size+77);
    }
}
let server = new Server();

process.stdin.resume();
process.stdin.setEncoding('utf8');
process.stdin.on('data', function (text) {
    if (text.trim() === 'status') {
        server.getServerStatus();
    }
    else if(text.trim() === 'exit'){
        datedLog('Stopping server');
        process.exit();
    }
});
