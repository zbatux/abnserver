export default class UniqueTuple {
    constructor(typeOfa, typeOfb, map) {
        if (map === undefined) {
            this.ab = new Map();
            this.ba = new Map();
            this.typeOfa = typeOfa;
            this.typeOfb = typeOfb;
        }
    }


    get typeOfTuple(){
        return {
            typeOfa : this.typeOfa,
            typeOfb : this.typeOfb
        }
    }

    get size() {
        return this.ab.size;
    }

    set(key, value, typeOfKey) {
        if (typeOfKey === this.typeOfa) {
            this.ab.set(key, value);
            this.ba.set(value, key);
        }
        else if (typeOfKey === this.typeOfb) {
            this.ab.set(value, key);
            this.ba.set(key, value);
        }
    }

    delete(key) {
        if (this.ab.has(key)) {
            this.ba.delete(this.ab.get(key));
            this.ab.delete(key);
        }
        else {
            this.ab.delete(this.ba.get(key));
            this.ba.delete(key);
        }
    }

    get(key) {
        if (this.ab.has(key)) return this.ab.get(key);
        else if (this.ba.has(key)) return this.ba.get(key);
        else return undefined;
    }


    getPair(key) {
        let pair = {};
        if (this.ab.has(key)) {
            pair[this.typeOfa] = key;
            pair[this.typeOfb] = this.ab.get(key);
        }
        else if (this.ba.has(key)){
            pair[this.typeOfa] = this.ab.get(key);
            pair[this.typeOfb] = key;
        }
        return pair;
    }

    has(key) {
        if (this.ab.has(key)) return true;
        else return this.ba.has(key);
    }
}
