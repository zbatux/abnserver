
const removeElement = (list, element) => {
    if(list.indexOf(element) !== -1) list.splice(list.indexOf(element),1);
    return list;
};

const datedLog = (msg) =>{
    return console.log('[' + new Date().toUTCString() + ']: ', msg) ;
};
module.exports = {removeElement, datedLog};
