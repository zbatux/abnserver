import { datedLog } from './misc/utils';
const pfs = {
    1: 'p',
    2: 's',
    3: 'f',
    4: 'm'
};

export default class Game {
    // constructor
    constructor(player1Id, player2Id, roomId, offline) {
        this.roomId = roomId;
        this.player1 = player1Id;
        this.player2 = player2Id;
        this.turnScore = [null, null];
        this.round = 0;
        this.gameScore = [0, 0];
        this.turnWinner = null;
        this.winner = null;
        this.offline = offline !== undefined ? offline : false;
    }


    turnOver(winner) {
        switch (winner) {
            case 0:
                break;
            case 1:
                this.gameScore[0]++;
                this.turnWinner = 0;
                break;
            case 2:
                this.gameScore[1]++;
                this.turnWinner = 1;
                break;
            default:
                break;
        }
        this.round++;
        if (this.gameScore[0] >= 3 || this.gameScore[1] >= 3) {
            this.gameOver();
        }
    }

    gameOver() {
        if(this.offline){
            if (this.gameScore[0] > this.gameScore[1]) this.winner = true;
            else if (this.gameScore[0] < this.gameScore[1]) this.winner = false;
            else this.winner = "THIS IS A BUG. WE NEED A WINNER";
        }
        else{
            if (this.gameScore[0] > this.gameScore[1]) this.winner = this.player1;
            else if (this.gameScore[0] < this.gameScore[1]) this.winner = this.player2;
            else this.winner = "THIS IS A BUG. WE NEED A WINNER";
        }
    }


    registerPlayerTurn(figure, playerId) {
        if(this.offline){
            this.turnScore[0] = figure;
            this.turnScore[1] = this.playIATurn();
        }
        else if (this.player1.localeCompare(playerId) === 0) {
            this.turnScore[0] = figure
        }
        else {
            this.turnScore[1] = figure
        }
        if (this.turnScore[0] != null && this.turnScore[1] != null) {
            this.computeTurnResult(this.turnScore[0], this.turnScore[1]);
        }

    }

    playIATurn(){
        const min = 1;
        const max = 4;
        return Math.floor(Math.random() * (max - min)) + min;
    }

    computeTurnResult(p1Value, p2Value) {
        //datedLog(pfs[p1Value] + ' vs ' + pfs[p2Value]);
        if(p2Value === 4 && p1Value === 4)
            this.turnOver(0);
        else if (p2Value === 4)
            this.turnOver(1);
        else if (p1Value === 4)
            this.turnOver(2);
        else if ((p1Value) % 3 + 1 === p2Value)
            this.turnOver(1);
        else if ((p2Value) % 3 + 1 === p1Value)
            this.turnOver(2);
        else
            this.turnOver(0);
    }

    getGameStateForPlayer(playerId) {
        if(this.offline) return {
            yourScore: this.gameScore[0],
            opponentScore: this.gameScore[1],
            round: this.round,
            turnResult: this.turnWinner === 0,
            myTurn: pfs[this.turnScore[0]],
            opponentTurn: pfs[this.turnScore[1]],
            gameWinner: this.winner,
            gameOver: this.winner != null
        };
        else if (this.player1.localeCompare(playerId) === 0) return {
            roomId: this.roomId,
            opponentId: this.player2,
            opponentScore: this.gameScore[1],
            yourScore: this.gameScore[0],
            round: this.round,
            turnResult: this.turnWinner === 0,
            myTurn: pfs[this.turnScore[0]],
            opponentTurn: pfs[this.turnScore[1]],
            gameWinner: this.winner ? this.winner.localeCompare(playerId) === 0 : false,
            gameOver: this.winner != null
        };
        else return {
                roomId: this.roomId,
                opponentId: this.player1,
                opponentScore: this.gameScore[0],
                yourScore: this.gameScore[1],
                round: this.round,
                turnResult: this.turnWinner === 1,
                myTurn: pfs[this.turnScore[1]],
                opponentTurn: pfs[this.turnScore[0]],
                gameWinner: this.winner ? this.winner.localeCompare(playerId) === 0 : false,
                gameOver: this.winner != null
            };

    }
}

