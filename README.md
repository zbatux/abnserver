# GJ The Game server	
GJ The Game is an online android mobile game.
This is the server that allow matchmaking and online play. 
More info about the game client [here](https://gitlab.com/zbatux/abn)

This app was made using Express with Socket.IO

## How to setup the server

 1. Upload the project on your server
 2. First install the app packages: `npm install`
 3. Run the server: `npm run start`


## Server commands:
While server is running you can use the following commands:

 - `status`: Allow you to retrieve: number of users connected, number waiting for a game, number of users playing, and number of game running.
 - `exit`: Stop the server
